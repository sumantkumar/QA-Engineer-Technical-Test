# QA Mobile Technical Test

## Overview

Welcome to the AirAsia QA Mobile Technical Test! This challenge involves automating test scenarios for the mobile app testing(Android & iOS) using your preferred programming language (TypeScript, JavaScript) and preferred test automation framework or library (WebdriverIO, Appium, BDD-Cucumber). 

The focus is on implementing automation tests following the Page Object Model (POM) and Data-Driven Testing (DDT) principles for better maintainability and readability.

## Getting Started

1. Read and comprehend the test automation challenge requirements provided in this repository.
2. Fork the original repository to your GitLab account to submit pull requests.
3. Create a New Branch to work on your changes.
4. Implement test automation according to the challenge requirements. Follow best practices for writing clear, maintainable, and efficient code.
5. Commit your changes with clear and concise commit messages representing logical steps in your development process.
6. Push your branch to your forked repository on GitLab.
7. Navigate to your forked repository on GitLab and create a new pull request from your feature branch to the original repository's main branch.
8. In the PR description, explain the changes made, the approach taken, and any challenges faced. Be clear and concise.

## The Challenge 1

#### Develop an Automation framework including 1 positive and one negative test scenario

### Continuous Integration

- The project should be integrated with (GitLab runner) for continuous integration.
- The automation suite is triggered on each push or pull request to the repository.

### Test Reports

- Test reports should be generated after each test run and can be found in the `/reports` directory.

### Issues and Challenges

- Document any challenges faced during the automation process and how they were addressed.

### Future Improvements

- Highlight any improvements or optimizations considered for future iterations.

### Bonus Points

- You can earn bonus points for:
  - Implementing parameterized tests.
  - Using environmental configurations.
  - Demonstrating knowledge of parallel test execution.


## The Challenge 2

#### Background: Imagine you are working as a Senior QA Engineer. The company provides mobile application for booking flight and hotel. Your team is responsible for creating an e2e test case that can be incorporated into a CICD pipeline to assure the functionality of the application with every release.

Tasks:

Identify and list all the test cases that should be included in e2e test suite to validate the mobile application's functionality.
Consider edge cases, negative scenarios, and potential issues that should be tested.
Suggest any areas or points for improvement that can make the release process less error-prone and more reliable.
Describe how this integration test suite can be incorporated into a CiCD pipeline for automated testing.

### Deliverables:

* A document or structured list that contains all the identified test cases with descriptions.
Suggestions for improving the reliability of the release process.
* A brief explanation of how the e2e test suite can be integrated into a Jenkins pipeline.
* Evaluation Criteria: Your test suite design and suggestions will be evaluated based on their comprehensiveness, attention to detail, and ability to ensure the reliability application's releases.

Feel free to ask any questions or seek clarifications if needed. Good luck!

## Good luck!

Best regards, 

AirAsia Group